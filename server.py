from flask import Flask
import os
from flask import Flask, jsonify, request
from werkzeug.utils import secure_filename
import mammoth
import json

from project_1 import p1_process
from project_2 import p2_process
from project_3 import process_excel
from project_4_v7 import p4_process_json
from project_5 import p5_process_file

UPLOAD_FOLDER = './uploads'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

if not os.path.exists(UPLOAD_FOLDER):
	os.mkdir(UPLOAD_FOLDER)

def allowed_file(filename, extensions):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in extensions

@app.route('/')
def hello():
    return 'Hello World'

@app.route('/project1', methods=['POST'])
def project1():
    if 'file' not in request.files:
        return jsonify({ 'error': 'No file provided' }), 400

    file = request.files['file']

    if file and allowed_file(file.filename, ['html', 'xls', 'xlsx']):
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)

        result = p1_process(path)

        return jsonify(result)

@app.route('/project2', methods=['POST'])
def project2():
    if 'file' not in request.files:
        return jsonify({ 'error': 'No file provided' }), 400

    file = request.files['file']

    if file and allowed_file(file.filename, ['doc', 'docx', 'html', 'xls', 'xlsx']):
        filename = secure_filename(file.filename)
        ext = filename.rsplit('.', 1)[1]
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)

        if ext == 'doc' or ext == 'docx':
            with open(path + '.html', 'wb') as wf:
                with open(path, 'rb') as f:
                    document = mammoth.convert_to_html(f)
                    wf.write(document.value.encode('utf8'))
                    path = path + '.html'

        result = p2_process(path)

        return jsonify(result)


@app.route('/project3', methods=['POST'])
def project3():
    if 'file' not in request.files:
        return jsonify({ 'error': 'No file provided' }), 400

    file = request.files['file']

    if file and allowed_file(file.filename, ['xls', 'xlsx']):
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)

        result = process_excel(path)

        return jsonify(result)

@app.route('/project4', methods=['POST'])
def project4():
    if 'file' not in request.files:
        return jsonify({ 'error': 'No file provided' }), 400

    file = request.files['file']

    url_api = request.form.get('URL_API', 'http://3.84.244.86:8080/enhancer/chain/PurchaseOrder')
    x_thresh = int(request.form.get('X_Threshold', 0))
    y_thresh = int(request.form.get('Y_Threshold', 10))
    word_special_chars = list(request.form.get('Word_Special_Character', '#*/!'))
    number_special_chars = list(request.form.get('Number_Special_Character', ',.'))
    required_urls = json.loads(request.form.get('Required_Urls', '[]'))

    if file and allowed_file(file.filename, ['json']):
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)

        result, debug, header_info = p4_process_json(path,
            url_api=url_api,
            x_thresh=x_thresh,
            y_thresh=y_thresh,
            word_special_chars=word_special_chars,
            number_special_chars=number_special_chars,
            required_urls=required_urls,
        )

        return jsonify({
            'result': result,
            'concatenation': debug,
            'header_info': header_info,
        })

        return jsonify(result)

@app.route('/project5', methods=['POST'])
def project5():
    if 'file' not in request.files:
        return jsonify({ 'error': 'No file provided' }), 400

    file = request.files['file']

    if file and allowed_file(file.filename, ['html', 'json', 'xls', 'xlsx']):
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)

        result = p5_process_file(path)

        return jsonify(result)



if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True, port=5015)

