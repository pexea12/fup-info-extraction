import json
import requests
import threading
import re
import traceback

MAX_THREADS = 50

URL = 'http://3.84.244.86:8080/enhancer/chain/PurchaseOrder'
FLATTEN_URL = 'http://34.80.26.185:8086/PO_Processing/GetDataSolr.flatteringTemp'
CHOOSE_URL = 'http://34.80.26.185:8086/PO_Processing/GetDataSolr.Temp'
VALIDATION_URL = 'http://34.80.26.185:8086/PO_Processing/ReArrangingofData'
REGEX_URL = 'http://35.221.160.146:5020/regexResult'
OVERLAP_URL = 'http://35.186.166.22:8082/portal/servlet/service/Poheader.poi'


LABEL = 'http://fise.iks-project.eu/ontology/entity-reference'
TEXT_LABEL = 'http://fise.iks-project.eu/ontology/selected-text'
RELATION_LABEL = 'http://purl.org/dc/terms/relation'
SELECTED_LABEL = 'http://bizlem.io/PurchaseOrderProcessing#'

def middle(value, left, right):
    return value >= left and value <= righft

def get_x(obj, index=0):
    return obj['bounding_poly']['vertices'][index]['x']

def get_y(obj, index=0):
    return obj['bounding_poly']['vertices'][index]['y']

def to_float(s):
    s_format = ''.join(re.split('[^\d\.]', s))
    return float(s_format)

def is_float(s):
    s_format = ''.join(re.split(',', s))
    try:
        num = float(s_format)
        return True
    except ValueError:
        return False


def parseJSON(data, x_thres, y_thres,
    word_special_chars=set(['+', '-', ';', '/', '\\', "'", '"']),
    number_special_chars=set([',', '.']),
):
    number_special_chars.add(',')
    number_special_chars.add('.')

    response = []

    # Build histogram
    y = [ (get_y(obj) + get_y(obj, 2)) // 2 for obj in data ]
    y.sort()

    cur_y = y[0]
    hist_y = { cur_y: cur_y }

    for yc in y:
        if yc == cur_y:
            continue
        if yc - cur_y > y_thres:
            cur_y = yc
        hist_y[yc] = cur_y

    data.sort(key=lambda obj: hist_y[(get_y(obj) + get_y(obj, 2)) // 2] * 1000000 + get_x(obj))


    curLineYCoord = (get_y(data[0]) + get_y(data[0], 2)) // 2
    curXEnd = get_x(data[0], 2)
    curXStart = get_x(data[0])
    line = {
        'y': curLineYCoord,
        'words': [],
    }

    completedWord = data[0]['description']
    characters = set(word_special_chars)
    open_chars = set(['(', '[', '{'])
    close_chars = set([')', ']', '}'])

    for i in range(1, len(data)):
        curWord = data[i]['description']
        yMid = (get_y(data[i]) + get_y(data[i], 2)) // 2
        xStart = get_x(data[i])


        if hist_y[yMid] <= hist_y[curLineYCoord] + y_thres and hist_y[yMid] >= hist_y[curLineYCoord] - y_thres:
            if xStart <= curXEnd + x_thres \
                or (xStart <= curXEnd + 2 * x_thres and ( \
                    (curWord[0] in characters) \
                    or curWord[0] in close_chars \
                    or ((curWord[0] in set(list(number_special_chars) + ['%'])) # need char % \
                        and '0' <= completedWord[-1] and completedWord[-1] <= '9') \
                    or completedWord[-1] in characters \
                    or completedWord[-1] in open_chars \
                    or (len(completedWord) >= 2 and completedWord[-1] in set(number_special_chars) \
                        and '0' <= curWord[0] and curWord[0] <= '9' \
                        and '0' <= completedWord[-2] and completedWord[-2] <= '9'))):
                    completedWord += curWord
                    curXEnd = get_x(data[i], 2)
            else:
                line['words'].append({
                    'word': completedWord,
                    'x1': curXStart,
                    'x2': curXEnd,
                })

                curXEnd = get_x(data[i], 2)
                completedWord = data[i]['description']
                curXStart = get_x(data[i])
        else:
            line['words'].append({
                'word': completedWord,
                'x1': curXStart,
                'x2': curXEnd,
            })

            response.append(line)
            curLineYCoord = (get_y(data[i]) + get_y(data[i], 2)) // 2
            curXStart = get_x(data[i])
            curXEnd = get_x(data[i], 2)

            completedWord = data[i]['description']
            line = {
                'y': curLineYCoord,
                'words': [],
            }

    return response

DEFAULT_REQUIRED_URLS = [
    'http://bizlem.io/PurchaseOrderProcessing#Invoice_Number',
    'http://bizlem.io/PurchaseOrderProcessing#Invoice_Date',
    'http://bizlem.io/PurchaseOrderProcessing#PAN_Number',
    'http://bizlem.io/PurchaseOrderProcessing#Purchase_Order_Number',
    'http://bizlem.io/PurchaseOrderProcessing#TAN',
    'http://bizlem.io/PurchaseOrderProcessing#GSTIN',
    'http://bizlem.io/PurchaseOrderProcessing#TRNO',
    'http://bizlem.io/PurchaseOrderProcessing#TRDate',
    'http://bizlem.io/PurchaseOrderProcessing#CIN',
    'http://bizlem.io/PurchaseOrderProcessing#VAT',
    'http://bizlem.io/PurchaseOrderProcessing#PODate',
]

DEFAULT_NUMBER_SPECIAL_CHARS = [',', '.']
DEFAULT_WORD_SPECIAL_CHARS = ['#', '*', '/', '!']
DEFAULT_URL_API = 'http://3.84.244.86:8080/enhancer/chain/PurchaseOrder'

def p7_process_json(path,
        verbose=True,
        url_api=DEFAULT_URL_API,
        x_thresh=0,
        y_thresh=10,
        word_special_chars=DEFAULT_WORD_SPECIAL_CHARS,
        number_special_chars=DEFAULT_NUMBER_SPECIAL_CHARS,
        required_urls=DEFAULT_REQUIRED_URLS,
    ):

    URL = url_api

    with open(path) as f:
        data = json.load(f)
        data_sum = data[0]
        data = data[1:]

    slist = parseJSON(data, x_thresh, y_thresh,
        word_special_chars=set(word_special_chars),
        number_special_chars=set(number_special_chars),
    )

    max_y = slist[-1]['y']
    max_x = 0
    for line in slist:
        if len(line['words']) == 0:
            continue

        last_word = line['words'][-1]['x2']
        if last_word > max_x:
            max_x = last_word


    sres = []
    sres_words = []

    for line in slist:
        sres.append({
            'y': line['y'],
            'words': [ word['word'] for word in line['words'] ],
        })

        sres_words.append({
            'y': line['y'],
            'words': [ word['word'] for word in line['words'] ]
        })


    def request_line_and_replace(slist, sres, sres_words, line_index):
        sentence = ' '.join([ word for word in sres[line_index]['words'] ])
        res = {}
        try: 
            r = requests.post(URL, data=sentence.encode('utf-8'), headers={'Content-Type': 'application/pdf'})
            r = r.json()

            for obj in r:
                if LABEL in obj and RELATION_LABEL in obj:
                    res[obj[LABEL][0]['@id']] = obj[RELATION_LABEL][0]['@id']

            for obj in r:
                if TEXT_LABEL in obj and '@id' in obj:
                    obj_id = obj['@id']
                    for kurl, vid in res.items():
                        if vid == obj_id:
                            res[kurl] = obj[TEXT_LABEL][0]['@value']
           
            merging_r = {}
            words = slist[line_index]['words']
            
            word_index = 0
            while word_index < len(words):
                word = words[word_index]
                found = False
                
                for kurl, vtext in res.items():
                    if kurl in merging_r:
                        continue
                        
                    if found:
                        break

                    vtext_first_word = vtext.split(' ')[0]
                    if vtext_first_word not in word['word']:
                        continue
                        
                    current_word_indexes = [ word_index ]
                    current_s = word['word']
                    current_i = word_index + 1
                    while len(current_s) < len(vtext) and current_i < len(words):
                        current_word_indexes.append(current_i)
                        current_s += ' ' + words[current_i]['word']
                        current_i += 1
                    
                    if vtext not in current_s:
                        continue
                    
                    found = True
                    merging_r[kurl] = current_word_indexes
                    
                if not found:
                    word_index += 1


            removed_words = set()
            for url, merging_arr in merging_r.items():
                min_index = merging_arr[0]
                start_word = slist[line_index]['words'][min_index]
                sres_words[line_index]['words'][min_index] = url

                for word_index in merging_arr[1:]:
                    removed_words.add(word_index)
                    current_word = slist[line_index]['words'][word_index]
                    start_word['word'] += ' ' + current_word['word']
                    start_word['x2'] = current_word['x2']

            slist_newarr = []
            sres_words_newarr = []

            for word_index, word in enumerate(slist[line_index]['words']):
                if word_index in removed_words: continue

                slist_newarr.append(word)
                sres_words_newarr.append(sres_words[line_index]['words'][word_index])

            slist[line_index]['words'] = slist_newarr
            sres_words[line_index]['words'] = sres_words_newarr

            sres[line_index]['words'] = list(res.keys())
    #         print(sres_words_newarr)

        except Exception as e:
            traceback.print_exc()


    def request_all(sres):
        threads = [ threading.Thread(target=request_line_and_replace, args=(slist, sres, sres_words, line_index)) for line_index in range(len(sres)) ]
        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    if verbose:
        print('requesting to get urls...')
    request_all(sres)
    if verbose:
        print('finish requesting...')


    date_regex = '\d{1,2}\s?[\.\-\/\:]\s?\d{1,2}\s?[\.\-\/\:]\s?\d{4}|\d{1,2}[\s\-]+[A-Za-z]{3,}[\s\-]+\d{4}|\d{1,2}\s?\/\s?\d{1,2}\s?\/\s?\d{2}'
    # (MM.DD.YYYY) | MM - DDD - YYYY | MM - DD - YYYY | MM / DD / YY
    number_regex = '(?<![\.\d])(?:\,? ?\d+ ?)+(?:\. ?\d+)?(?!\d{0,} ?%)'
    percentage_regex = '\d+\s?%|\d+\s?\.\s?%'


    # Regex part
    def concat_rows_and_cols(slist, line_index, word_index, n_lines=10, no_limit=False):
        paragraph = []
        for word in slist[line_index]['words'][word_index:]:
            nword = word.copy()
            nword['y'] = slist[line_index]['y']
            nword['line_index'] = line_index
            paragraph.append(nword)
        
        current_word = slist[line_index]['words'][word_index]
        
        for lindex in range(line_index + 1, min(line_index + n_lines + 1, len(slist))):
            line = slist[lindex]
            for word in line['words']:
                if no_limit or (word['x1'] >= current_word['x1'] - 15 and word['x1'] <= current_word['x2']):
                    nword = word.copy()
                    nword['y'] = line['y']
                    nword['line_index'] = lindex
                    paragraph.append(nword)
        
        return paragraph

    def request_regex_word(word, word_index, container):
        try:
            r = requests.post(REGEX_URL, data=word['word'].encode('utf-8'))
            container[word_index] = r.json()
        except Exception as e:
            traceback.print_exc()

    def request_regex_header(slist, line_index, word_index, word_url, paragraph, regex_res):
        container = [ None ] * len(paragraph)

        for i in range(0, len(paragraph), MAX_THREADS):
            threads = [ 
                threading.Thread(
                    target=request_regex_word, 
                    args=(paragraph[word_index], word_index, container),
                ) for word_index in range(i, min(i + MAX_THREADS, len(paragraph))) 
            ]

            for thread in threads:
                thread.start()
        
            for thread in threads:
                thread.join()

        regex_res.setdefault(word_url, {
            'Words': [],
            'Regex': [],
            'Integer': [],
            'Date': [],
            'UKS': [],
        })
        
        current_word = slist[line_index]['words'][word_index]

        for i in range(max(0, line_index - 2), min(len(slist), line_index + 2)):
            for word in slist[i]['words']:
                regex_res[word_url]['Words'].append({
                    'Value': word['word'],
                    'Length': len(word['word']),
                    'Line_Offset': i - line_index,
                    'X1 OffSet': word['x1'] - current_word['x1'],
                    'X1-X2 OffSet': word['x1'] - current_word['x2'],
                    'Y OffSet': slist[i]['y'] - slist[line_index]['y'],
                    'X2-X1 OffSet': word['x2'] - current_word['x1'],
                    'X2-X2 OffSet': word['x2'] - current_word['x2'],
                })

        for rr_index, rr in enumerate(container):
            word = paragraph[rr_index]
            for item in rr:
                regex_res[word_url]['Regex'].append({
                    'Regex_Type': item['Regex_Type'],
                    'Regex_Value': item['value'],
                    'Regex_Id': item['Regex_id'],
                    'Regex_Length': item['length'],
                    'Regex_Line_OffSet': word['line_index'] - line_index,
                    'Regex_X1 OffSet': word['x1'] - current_word['x1'],
                    'Regex_X1-X2 OffSet': word['x1'] - current_word['x2'],
                    'Regex_Y OffSet': word['y'] - slist[line_index]['y'],
                    'Regex_X2-X1 OffSet': word['x2'] - current_word['x1'],
                    'Regex_X2-X2 OffSet': word['x2'] - current_word['x2'],
                })
                
        for word in paragraph:
            search_result = re.search('\d+', word['word'])
            if search_result is not None:
                regex_res[word_url]['Integer'].append({
                    'Integer_Value': word['word'][search_result.start():search_result.end()],
                    'Integer_Length': search_result.end() - search_result.start(),
                    'Integer_Line_OffSet': word['line_index'] - line_index,
                    'Integer_X1 OffSet': word['x1'] - current_word['x1'],
                    'Integer_X1-X2 OffSet': word['x1'] - current_word['x2'],
                    'Integer_Y OffSet': word['y'] - slist[line_index]['y'],
                    'Integer_X2-X1 OffSet': word['x2'] - current_word['x1'],
                    'Integer_X2-X2 OffSet': word['x2'] - current_word['x2'],
                })

            search_result_date = re.search(date_regex, word['word'])
            if search_result_date is not None:
                regex_res[word_url]['Date'].append({
                    'Date_Value': word['word'][search_result_date.start():search_result_date.end()],
                    'Date_Length': search_result_date.end() - search_result_date.start(),
                    'Date_Line_OffSet': word['line_index'] - line_index,
                    'Date_X1 OffSet': word['x1'] - current_word['x1'],
                    'Date_X1-X2 OffSet': word['x1'] - current_word['x2'],
                    'Date_Y OffSet': word['y'] - slist[line_index]['y'],
                    'Date_X2-X1 OffSet': word['x2'] - current_word['x1'],
                    'Date_X2-X2 OffSet': word['x2'] - current_word['x2'],
                })


    regex_urls = required_urls


    threads = []
    regex_res = {}

    for line_index, line in enumerate(slist):
        for word_index, word in enumerate(line['words']):
            word_url = sres_words[line_index]['words'][word_index]
            if word_url in regex_urls:
                paragraph = concat_rows_and_cols(slist, line_index, word_index)
                
                threads.append(threading.Thread(
                    target=request_regex_header, 
                    args=(slist, line_index, word_index, word_url, paragraph, regex_res),
                ))
    
    # requesting normal regex
    if verbose:
        print('requesting for normal regex...', len(threads))

    for thread in threads:
        thread.start()
        
    for thread in threads:
        thread.join()

    if verbose:
        print('finish requesting for normal regex...')


    # requesting all regex
    if verbose: 
        print('requesting all regex...')

    full_paragraph = concat_rows_and_cols(slist, 0, 0, n_lines=1000000, no_limit=True)
    request_regex_header(slist, 0, 0, 'http://bizlem.io/PurchaseOrderProcessing#AllRegex', full_paragraph, regex_res)

    if verbose:
        print('finish requesting for all regex...')

    if verbose:
        print('finish requesting for regex')

    header_info = {
        'Rules': regex_res,
    }       

    return header_info


if __name__ == '__main__':
    header_info = p7_process_json('p4materials/c47.json', verbose=True)

    print(json.dumps(header_info, indent=2))

