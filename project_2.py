import requests
import pandas as pd
import json
from bs4 import BeautifulSoup
import re
import html2text
import threading


URL = 'http://35.188.227.39:8080/enhancer/chain/EssarApproval'
TITLE_STATUS = 'http://fise.iks-project.eu/ontology/entity-label'
TITLE = 'http://www.w3.org/2000/01/rdf-schema#label'

def is_int(s):
    try:
        num = int(s)
        return True
    except ValueError:
        return False

def is_str(s):
    return isinstance(s, str)

def strip(s):
    return ''.join(re.split('[^a-zA-Z]', s.lower()))

# crawl variation of labels
print('Requesting server for labels...')

def request_label(label):
    r = requests.post(URL, data=label, headers={'Content-Type': 'application/pdf'})
    res = r.json()
    for v in res[0][TITLE]:
        labels[strip(v['@value'])] = label


default_labels = [
    'SO',
    'Item',
    'Remark',
]

labels = {}

threads = [ threading.Thread(target=request_label, args=(label,)) for label in default_labels ]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

labels['status'] = 'Remark'
labels['Status'] = 'Remark'


status_list = ['hold', 'cleared']
# extract text
def extract_text(path):
    with open(path) as f:
        soup = BeautifulSoup(f, features='lxml')

    for t in soup.find_all('table'):
        t.extract()

    text = re.split('[^a-zA-Z0-9 ]', soup.text)
    text = [ t for t in text if len(t) > 0 ]
    status = None

    for t in text:
        if status is not None:
            break

        r = requests.post(URL, data=t, headers={'Content-Type': 'application/pdf'})
        res = r.json()

        for v in res:
            if TITLE_STATUS in v:
                s = v[TITLE_STATUS][0]['@value']
                if s in status_list:
                    status = s
                    break

    return status


def process_no_table(path):
    with open(path) as f:
        text = html2text.html2text(f.read())
   
    text = [ t.strip() for t in re.split('[^a-zA-Z0-9 \:\-]', text) if len(t) > 0 ]

    results = []
    
    global_status = { 'value': None }

    def request_text(t, results, global_status):
        status = None
        r = requests.post(URL, data=t, headers={'Content-Type': 'application/pdf'})
        res = r.json()

        for v in res:
            if TITLE_STATUS in v:
                s = v[TITLE_STATUS][0]['@value']
                if s in status_list:
                    status = s
        
        words = re.split('[^a-zA-Z0-9]', t)
        
        obj = { 'Remark': status }
        for word in words:
            if is_int(word):
                if len(word) == 10 or (word[0] == '-' and len(word) == 11):
                    obj['SO'] = word
                else: 
                    obj['Item'] = word

        if 'SO' in obj:
            results.append(obj)
        
        if status is not None and global_status['value'] is None:
            global_status['value'] = status

    threads = [ threading.Thread(target=request_text, args=(t, results, global_status)) for t in text ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    for obj in results:
        if obj['Remark'] is None:
            obj['Remark'] = global_status['value']

    return results

def process_table(table, status):
    table.dropna(axis=0, how='all', inplace=True)
    table.dropna(axis=1, how='all', inplace=True)

    table.columns = table.iloc[0]
    height, width = table.shape

    label_so = None

    for col in table.columns:
        if label_so is not None:
            break

        for k, v in labels.items():
            if v == 'SO' and is_str(col) and k == strip(col):
                label_so = col
                break

    results = []
    for i in range(height):
        if is_int(table[label_so].iloc[i]):
            obj = {}
            for col in table.columns:
                if is_str(col) and strip(col) in labels:
                    obj[labels[strip(col)]] = table[col].iloc[i]

            if 'Remark' not in obj:
                obj['Remark'] = status

            results.append(obj)

    return results

# create json file
def p2_process(path, verbose=True):
    tables = []
    status = None

    try:
        ext = path.split('.')[-1]

        if ext == 'html':
            tables = pd.read_html(path)
            tables = tables[0:1]
            status = extract_text(path)

        elif ext == 'xls' or ext == 'xlsx':
            xls = pd.ExcelFile(path)
            sheets = xls.book.sheets()

            # read visible sheet only
            tables = []
            for sheet in sheets:
                if sheet.visibility == 0:
                    table = pd.read_excel(xls, sheet.name, header=None)
                    th, tw = table.shape

                    if th > 0 and tw > 0:
                        tables.append(table)

            status = None

    except ValueError:
        tables = []

    if len(tables) == 0:
        results = process_no_table(path)
        return { 'SOITEMS': results }
    

    results_table = [ process_table(table, status) for table in tables ]
    results = []
    for r in results_table:
        results += r

    final = { 'SOITEMS': results }

    if verbose:
        print('Found', len(results), 'record(s)')
    return final

if __name__ == '__main__':
    r = p2_process('p2materials/p22.html')
    print(json.dumps(r, indent=2))
