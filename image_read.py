import ast
import datetime
import re
import requests
import os
import io
import json
from flask import Flask, request, jsonify
# from google.cloud import vision
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types
from google.cloud.vision_v1 import enums
from google.protobuf.json_format import MessageToJson,MessageToDict



app = Flask(__name__)

@app.route("/",methods=["GET"])

def excel_parse():
	file_name=request.args.get('file')
	returned_json={}
	
	# if ext in ["png","jpg","jpeg"]:
		# print "fn--",fname
		# client = vision.ImageAnnotatorClient()
		# response = client.annotate_image({
		  # 'image': {'source': {'image_uri': 'gs://my-test-bucket/image.jpg'}},
		  # 'features': [{'type': vision.enums.Feature.Type.FACE_DETECTION}],
		# })	
		# return "ok"
	# else:	
		# return "invalid file type"
	# df2.to_csv(output_path2,encoding='utf-8',index=False)
	

	# Instantiates a client
	client = vision.ImageAnnotatorClient()

	# Loads the image into memory
	with io.open(file_name, 'rb') as image_file:
		content = image_file.read()
		
	requests=[]
	image = types.Image(content=content)
	# requests.append({'image': {'content': content,},'features': [{'type': enums.Feature.Type.TEXT_DETECTION,}, {'type': enums.Feature.Type.WEB_DETECTION,}]})
	requests.append({'image': {'content': content,},'features': [{'type': enums.Feature.Type.TEXT_DETECTION}]})
	responseNew = client.batch_annotate_images(requests)
	# Performs label detection on the image file
	# response2 = client.label_detection(image=image)
	response2 = client.text_detection(image=image)
	# code 2==============
	features = [types.Feature(type=enums.Feature.Type.TEXT_DETECTION)]

	requests_arry = []
	# for filename in ['foo.png', 'bar.jpg', 'baz.gif']:
	# with open(filename, 'rb') as image_file:
	image2 = types.Image(content = content)
	request_ = types.AnnotateImageRequest(image=image2, features=features)
	requests_arry.append(request_)

	response_data = client.batch_annotate_images(requests_arry)

	# for annotation_response in response_data.responses:
	   # print ("datt--",annotation_response)
	
	# end code2========================
	
	# labels = response.label_annotations
	
	response = client.document_text_detection(image=image)
	labels = response.full_text_annotation
	# text_ann=response.textAnnotations
	# print ("rtype--",type(response))
	
	returned_json.update({"textAnnotations":[{"description":labels.text.splitlines()}]})
	serialized = MessageToJson(responseNew)
	jsonserialised=json.loads(serialized)
	
	# serialized2 = MessageToJson(str(response))
	# jsonserialised2=json.loads(response)
	response22 = MessageToDict(response, preserving_proto_field_name = True)
	desired_res = response22["text_annotations"]
	# print ("ttu---",type(desired_res))
	# code to arrangtext
	finallist=[]
	lst=[]
	y_arrylst=[]
	x_arrylst=[]
	all_x=[]
	all_y=[]
	flag=False
	xflag=False
	final_datalist=[]
	if isinstance(desired_res,list):
		for index,item in enumerate(desired_res):
			if isinstance(item,dict):
				# lst=[]
				# dic={}
				if "bounding_poly" in item.keys():
					# print ("item--",item)
					if isinstance(item["bounding_poly"],dict):
						# print ("ss--",item["bounding_poly"])
						if "vertices" in item["bounding_poly"].keys():
							# print ("aa--")
							if isinstance(item["bounding_poly"]["vertices"],list):
								if len(item["bounding_poly"]["vertices"])==4:
									lst=[]
									dic={}
									for ind,itm in enumerate(item["bounding_poly"]["vertices"]):
										flag=False
										xflag=False
										if isinstance(itm,dict):
											# print ("bb--",itm)
											# if ind==0:
											if "x" in itm.keys():
												dic.update({"x"+str(ind+1):itm["x"]})
												if ind==2:
													if int(itm["x"]) not in x_arrylst:
														for i in x_arrylst:
															if int(itm["x"])-i in range(2,-3,-1):
																xflag=True
														if not xflag:
															x_arrylst.append(itm["x"])
											if "y" in itm.keys():
												dic.update({"y"+str(ind+1):itm["y"]})
												# if itm["y"] not in y_arrylst:
													# if itm["y"]-1 not in y_arrylst or itm["y"]+1 not in y_arrylst:
														# y_arrylst.append(itm["y"])
												if ind==3:
													if int(itm["y"]) not in y_arrylst:
														for i in y_arrylst:
															if int(itm["y"])-i in range(11,-12,-1):
																flag=True
														if not flag:
															y_arrylst.append(itm["y"])
																				
									if "description" in item.keys():
										# print ("desc--",item["description"])
										dic.update({"text":item["description"]})
										
									if dic:
										if "x2" in dic.keys() and "x1" in dic.keys():
											dic.update({"width":dic['x2']-dic['x1']})
										if "y3" in dic.keys() and "y2" in dic.keys():
											dic.update({"height":dic['y3']-dic['y2']})
										# lst.append(dic)
									# if finallist:
										# t=len(finallist)-1
										# if t>0:
											# if "x2" in finallist[t].keys():
												# if "x1" in dic.keys():
													# dic.update({"word_space":int(dic['x1'])-int(finallist[t]['x2'])})
													# # print ("diff--",int(dic['x1']),int(finallist[t]['x2']),finallist[t])
													# print ("fin--",finallist[t])
													# print ("dic--",dic)
									finallist.append(dic)
				# if lst:
					# # print ("ff--",lst)
					# finallist.append(lst)
					
	tmplst=sorted(finallist, key = lambda i: i['y4'])		
	y_row_arry=[]
	for index,itm in enumerate(tmplst):
		if index>0 and index!=len(tmplst)-1:
			if "x1" in itm.keys():
				if "x2" in tmplst[index-1].keys():
					va=int(itm['x1'])-int(tmplst[index-1]['x2'])
					if va>=0:
						itm.update({"word_space":int(itm['x1'])-int(tmplst[index-1]['x2'])})
					elif va<0:
						itm.update({"word_space":0})
						if itm['y4'] not in y_row_arry:
							y_row_arry.append(itm['y4'])
		else:
			itm.update({"word_space":0})
			if itm['y4'] not in y_row_arry:
				y_row_arry.append(itm['y4'])
	
	tmp=[]	
	finaltmp=[]
	finaltmp_text=[]
	# for index,itm in enumerate(tmplst):
		# if 'word_space' in itm.keys():
			# if itm['word_space']==0:
				# if tmp:
					# finaltmp.append(tmp)
					# finaltmp_text.append([ele['text'] for ele in tmp])
					# # print ("finaltmp_text--",finaltmp_text)
					# tmp=[]
				# tmp.append(itm)
			# elif itm['word_space']!=0:
				# tmp.append(itm)
				
	row=[]
	row2=[]
	tmp_row2=[]
	row_lst=[]
	row_lst2=[]
	print ("ylst-",sorted(y_arrylst))
	sortedyarry=sorted(y_arrylst)
	# sortedyarry=sorted(y_arry)
	y_row_arry=sorted(y_row_arry)
	print ("yarry--",y_row_arry)
	inserted_ele=[]
	y_arr=sorted
	final_datalist_ele=[]
	for y in sortedyarry:
		if y!=max(sortedyarry):
			if row:
				row_lst.append(row)
				row_lst2.append(tmp_row2)
			row=[]
			row2=[]
			tmp_row2=[]
			# for itms in finallist:
			for ind,ele in enumerate(tmplst):
				if "y4" in ele.keys():
					# if int(ele["y4"])==y or int(ele["y4"])-y in range(10,-11,-1):
					if int(ele["y4"])==y or int(ele["y4"])-y in range(11,-12,-1):
						row.append(ele["text"])
						if ind not in inserted_ele:
							row2.append(ele)		
							inserted_ele.append(ind)
							
			# tmp_row2=sorted(row2,key = lambda i: i['x3'])
			tmp_row2=sorted(row2,key = lambda i: i['x1'])
			f=""
			for nd,i in enumerate(tmp_row2):
				if "word_space" in i.keys():
					if i["word_space"]<=10:
						if nd!=0:
							f=tmp_row2[nd-1]['text']+i['text']
			# print ("word--",f)
			# if tmp_row2:print ("-"," ".join([ele["text"] for ele in tmp_row2]))
			final_datalist_ele.append(tmp_row2)
			if tmp_row2:
				# final_datalist.append([ele["text"] for ele in tmp_row2])
				final_datalist.append(list(" ".join([ele["text"] for ele in tmp_row2])))
	# =======================================================================
	# for indx,y in enumerate(y_row_arry):
		# if y!=max(y_row_arry):
			# # row=[]
			# row2=[]
			# tmp_row2=[]
			# # for itms in finallist:
			# for ind,ele in enumerate(tmplst):
				# if "y4" in ele.keys():
					# if int(ele["y4"])==y:
						# if row2:
							# tmp_row2=[]
							# tmp_row2=sorted(row2,key = lambda i: i['x1'])
							# # if tmp_row2:print ("-"," ".join([ele["text"] for ele in tmp_row2]))
							# if tmp_row2:
								# print ("-"," ".join([ele["text"] for ele in tmp_row2]))
								# final_datalist.append([ele["text"] for ele in tmp_row2])
						
						# row2=[]
						# row2.append(ele)		
					# else:
						# row2.append(ele)		
	
	# ==============================
	# for x in sortedyarry_x:
		# for y in sortedyarry:
			# if row:
				# row_lst.append(row)
			# row=[]
			# for itms in finallist:
				# for ele in itms:
					# if "x3" in ele.keys():
						# if int(ele["x3"])==y or int(ele["x3"])-x in range(2,-3,-1):
							# if "y4" in ele.keys():
								# if int(ele["y4"])==y or int(ele["y4"])-y in range(3,-4,-1):
									# row.append(ele["text"])
									# print (row)
									# # row.append(y)
			# # if row:print ("-","".join(row))
			# if row:print ("-",row)
	# end code to arrangtext
	# print ("flst--",finallist)
	# return jsonify(jsonserialised)
	# return jsonify(row_lst)
	po=['ESTL','ESTLI']
	gstin=['GSTIN','GSTN']
	cgst=['CGST','C.G.S.T.']
	sgst=['SGST','S.G.S.T.']
	total=['Total','total','TOTAL','Gross Total','Net Amount']
	rs=['Rs','Rs.']
	pan=['PAN']
	panno=['NO','NO.','No','No.']
	# gstpattern = re.compile(r"([A-Z0-9]{15})")
	datadic={}
		# { 
	# "Invoice_No": "GST/18-19/17019", 
	# "Vendor_Name": "SEA LINKERS PVT.LTD", // same as sending party
	# "Bill_To" : "",
	# "Ship_To" : "",
	# "Invoice_Date": "18/12/2018", 
	# "PAN_NO": "AAHCS5084N", 
	# "TAN_NO": "AAHC355B", 
	# "PO": "ESTL-DTA.MAH.4500251401", 
	# "PONumber": "ESTL-DTA.MAH.4500251401", 
	# "GRN": "", 
	# "GSTTIN": "27AAHCS5084N1ZO", 
	# "ServiceTaxNo": "FGSS453BFD", 
	# "SGST": "", 
	# "CGST": "", 
	# "IGST": "", 
	# "GRAND_TOTAL": "", 
	# "itemdescription":[
	total_amt=[]
	gsti_arry=[]
	# sample_json.update({"PONumber":"ESTL4500987","INVOICE_NO":"VC1819","Invoice_Date":"01/10/2019","INVOICE_TO":"xyz","Sending_Party":"ABC","Bill_To":"aaa","Ship_To":"zzz","SERVICES_TAX_REG_NO":"NM1234","PAN_NO":"CF879Q99","GSTTIN":"aa7773","CSTTIN":"hh9776","SR_NO":"1","DECCRIPTION_OF_SERVICES":"asas","Rate":"20.00","Qty":"2","Amount":"200","Tax":"2","Invoice":"s222","PO":"ESTL4500987","GRN":"hhh2322","Payable":"220.00","Total":"450.00","Taxable_Value":"100.00","GST":"2","CST":"2","GRAND_TOTAL":"220.00"})
	
	tmp_datalst=[]
	
		
	# for item in row_lst2:
		# tmp=[itm["text"] for itm in item]
		# tmp2="".join(tmp)
		# if any(ele in po for ele in tmp):
			# popatt=re.findall(r"(ESTL.*)",str(tmp2))
			# if popatt:
				# # print ("po--",("".join(tmp)),popatt)
				# datadic.update({"PONumber":popatt[0]})
		# elif any(ele in gstin for ele in tmp):
			# # print ("gstin--","".join(tmp))
			# gsti=re.findall(r"([A-Z0-9]{15})",str(tmp2))
			# if gsti:
				# gsti_arry.append(gsti)
				# datadic.update({"GSTTIN":gsti_arry})
				# # print ("gsti1-",gsti_arry)
		# elif any(ele in cgst for ele in tmp):
			# # print ("cgst--","".join(tmp))
			# cgstper1=re.findall(r"([0-9]{1,2}%)",str(tmp2))
			# cgstper2=re.findall(r"([0-9]{1,2}\.[0-9]{2}%)",str(tmp2))
			# cgst_amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 22,333.00
			# cgst_amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 13567.80
			# cgst_amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(tmp2))
			# # 3,600
			# cgst_amount4=re.findall(r"([0-9]{1,}\,[0-9]{3})",str(tmp2))
			# # print ("cgst--",cgst_amount,cgst_amount2,cgst_amount3,cgst_amount4)
			# # print ("cgst--",cgstper1)
			# # if cgstper1:
				# # print ("cgstper--",cgstper1)
			# # elif cgstper2:
				# # print ("cgstper2--",cgstper)
			# if cgst_amount:
				# datadic.update({"GST":cgst_amount[0]})
			# elif cgst_amount2:
				# datadic.update({"GST":cgst_amount2[0]})
			# elif cgst_amount3:
				# datadic.update({"GST":cgst_amount3[0]})
			# elif cgst_amount4:
				# datadic.update({"GST":cgst_amount4[0]})
			
		# elif any(ele in sgst for ele in tmp):
			# # print ("sgst--","".join(tmp))
			# sgstper1=re.findall(r"([0-9]{1,2}%)",str(tmp2))
			# sgstper2=re.findall(r"([0-9]{1,2}\.[0-9]{2}%)",str(tmp2))
			# sgst_amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 22,333.00
			# sgst_amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 13567.80
			# sgst_amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(tmp2))
			# # 3,600
			# sgst_amount4=re.findall(r"([0-9]{1,}\,[0-9]{3})",str(tmp2))
			# # print ("cgst--",sgst_amount,sgst_amount2,sgst_amount3,sgst_amount4)
			# if sgst_amount:
				# datadic.update({"CST":sgst_amount[0]})
			# elif sgst_amount2:
				# datadic.update({"CST":sgst_amount2[0]})
			# elif sgst_amount3:
				# datadic.update({"CST":sgst_amount3[0]})
			# elif sgst_amount4:
				# datadic.update({"CST":sgst_amount4[0]})
			# # if sgstper1:
				# # print ("sgstper--",sgstper1)
		# elif any(ele in total for ele in tmp):
			 # # and any(ele in rs for ele in tmp)
			# # print ("total--","".join(tmp))
			# # 1,22,300.00
			# amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 22,333.00
			# amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(tmp2))
			# # 13567.80
			# amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(tmp2))
			
			# # print ("aa--",amount,amount2)
			# if amount:
				# # print ("amt--",amount)
				# total_amt.append(amount[0])
			# elif amount2:
				# # print ("amt2--",amount2)
				# total_amt.append(amount2[0])
			# elif amount3:
				# # print ("amt3--",amount3)
				# total_amt.append(amount3[0])
			# if total_amt:
				# datadic.update({"Amount":total_amt})
		# elif any(ele in pan for ele in tmp) and any(ele in panno for ele in tmp):
			# # print ("panno--","".join(tmp))
			# panpatt=re.findall(r"([A-Z]{5}[0-9]{4}[A-Z]{1})",str(tmp2))
			# if panpatt:
				# # print ("p--",panpatt[0])
				# datadic.update({"PAN_NO":panpatt[0]})
				
	# return jsonify(datadic)
	# print ("dicbef--",datadic)
	final_datalist2=[]
	final_datalist3=[]
	# return jsonify(final_datalist)
	final_datalist2=stanbol(final_datalist)
	# regex is used in make json method
	final_datalist3=make_json(final_datalist2)
	# return jsonify(desired_res)
	# return jsonify(tmplst)
	return jsonify(final_datalist2)
	
def stanbol(final_datalist):
	api_url="http://35.188.227.39:8080/enhancer/chain/ReceiptChain"
	header={ "Content-Type": "application/pdf"}
	r=""
	stanboldic_r=""
	stanboldic_r_copy=""
	replaced_url=""
	keyreplaced=""
	digitvalue=""
	st=""
	tmp_final_datalist=[]
	id_list=[]
	for index,ele in enumerate(final_datalist):
		flag=False
		if isinstance(ele,list):	
			tmp_final_datalist.append("".join(ele))
			r = requests.post(url=api_url,headers=header,data= "".join(ele).encode('utf-8'))
			if r:
				stanboldic_r = ast.literal_eval(r.text)
				stanboldic_r_copy=stanboldic_r
			if stanboldic_r:
				# print ("list ele--","".join(ele).encode('utf-8'))
				for item in stanboldic_r:
					if isinstance(item,dict):
						# print ("item--",item)
						for key,val in item.items():
							if key=="@type":								
								if "http://fise.iks-project.eu/ontology/EntityAnnotation" in val:
									s=item['http://fise.iks-project.eu/ontology/entity-label']
									entity_type=item["http://fise.iks-project.eu/ontology/entity-type"]
									# if isinstance(entity_type,list):
										# if entity_type:
											# for x in entity_type:
												# tmp=x
												# if isinstance(tmp,dict):
													# for q,r in tmp.items():
														# if q=="@id":
															# replaced_url=r
										
									if isinstance(s,list):
										for l in s:
											if isinstance(l,dict):
												for a,b in l.items():
													if a=="@value":
														# if b.lower().strip()==i.lower().strip():
														z=item["http://fise.iks-project.eu/ontology/entity-reference"]
														id=item['http://purl.org/dc/terms/relation']
														if isinstance(z,list):
															if z:
																tmpdict=z[0]
																if isinstance(tmpdict,dict):
																	for q,r in tmpdict.items():
																		if q=="@id":
																			keyreplaced=r
																			# print ("\nurl---",keyreplaced)
										# if flag==False:						
										if isinstance(id,list):
											# print ("d--",id)
											for l in id:
												if isinstance(l,dict):
													for a,b in l.items():
														if a=="@id":
															digitvalue=b
															# print "sele text--",digitvalue
															for item in stanboldic_r_copy:
																if isinstance(item,dict):
																	for key,val in item.items():
																		if key=="@id":
																			if val==digitvalue:
																				d=item["http://fise.iks-project.eu/ontology/selected-text"]
																				# elif "http://fise.iks-project.eu/ontology/TextAnnotation" in val:
																				if isinstance(d,list):
																					for l in d:
																						if isinstance(l,dict):
																							for a,b in l.items():
																								if a=="@value":
																									st=str(tmp_final_datalist[index])
																									tmp_final_datalist[index]=st.replace(b,keyreplaced)
																									# print "\nst----",(st.replace(b,b.replace(" ","_")))
																									# print ("\nb----",b,keyreplaced)
																									flag=True
																														
																																									
	return tmp_final_datalist														

def make_json(tmp_final_datalist):	
	datadic={}
	urlpart=""
	flag=False
	headers=[]
	description=[]
	amounts=[]
	net_amt=[]
	headers_index=[]
	non_headers_index=[]
	# regex for various fields are applied below
	for index,item in enumerate(tmp_final_datalist):
		urlpatt=re.findall(r"(\http?s?:.*\#)",str(item))
		if urlpart+"Tax_Invoice" in item or urlpart+"GST_Invoice" in item:
			flag=True
	print ("flag--",flag)
	if flag==True:	
		for index,item2 in enumerate(tmp_final_datalist):
			item=item2.replace(" ","")
			# print ("item--",str("".join(item)))
			print ("item2--",item2)
			rates=re.findall(r"(\s\d{1,2}\s\.\s\d{2})",str(item2))
			am=re.findall(r"(\s\d{2,}\s\.\s\d{2})",str(item2))
			rate_arry=[]
			g=[]
			g2=[]
			if rates:
				g=list(map(float,[ele.replace(" ","") for ele in rates]))
				print ("g--",g)
			if am:
				g2=list(map(float,[ele.replace(" ","") for ele in am]))
				print ("g2--",g2)
			if g:
				rate_arry.extend(g)
			if g2:
				rate_arry.extend(g2)
			print ("ratarry--",rate_arry)
			urlpatt=re.findall(r"(\http?s?:.*\#)",str(item))
			panpatt=re.findall(r"([A-Z]{5}[0-9]{4}[A-Z]{1})",str(item))
			amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 22,333.00
			amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 13567.80
			amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(item))
			amount4=re.findall(r"(\d{3}\.\d{2})",str(item))
			
			# print ("amount--",amount,amount2,amount3,amount4)
			sgstper1=re.findall(r"([0-9]{1,2}%)",str(item))
			sgstper2=re.findall(r"([0-9]{1,2}\.[0-9]{2}%)",str(item))
			sgst_amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 22,333.00
			sgst_amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 13567.80
			sgst_amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(item))
			# 3,600
			sgst_amount4=re.findall(r"([0-9]{1,}\,[0-9]{3})",str(item))
			# print ("sgst--",sgst_amount,sgst_amount2,sgst_amount3,sgst_amount4)
			cgstper1=re.findall(r"([0-9]{1,2}%)",str(item))
			cgstper2=re.findall(r"([0-9]{1,2}\.[0-9]{2}%)",str(item))
			cgst_amount=re.findall(r"(\d{1,2}\,\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 22,333.00
			cgst_amount2=re.findall(r"(\d{1,2}\,\d{3}\.\d{2})",str(item))
			# 13567.80
			cgst_amount3=re.findall(r"([0-9]{4,}\.\d{2})",str(item))
			# 3,600
			cgst_amount4=re.findall(r"([0-9]{1,}\,[0-9]{3})",str(item))
			# print ("cgst--",cgst_amount,cgst_amount2,cgst_amount3,cgst_amount4)
			popatt=re.findall(r"(ESTL.*)",str(item))
			gstin=re.findall(r"([A-Z0-9]{15})",str(item))
			invoice=re.findall(r"(\d{1,})",str(item))
			# date format
			dt=re.findall(r"(\d{1,2}th\s([A-Za-z]{3,})\s\d{4})",str(item))
			dt2=re.findall(r"(\d{1,2}\s\[A-Za-z]{3,}\s\d{4})",str(item))
			dt3=re.findall(r"(\d{1,2}\s[A-Za-z]{3,}\s\d{4})",str(item))
			dt4=re.findall(r"(\d{1,2}-[A-Za-z]{3,}-\d{4})",str(item))
			dt5=re.findall(r"(\d{1,2}\.\d{1,2}\.\d{2})",str(item))
			dt6=re.findall(r"(BY MONTH\s/\s\d{4}-\d{4})",str(item))
			dt7=re.findall(r"(\d{1,2}\/\d{1,2}\/\d{4})",str(item))
			dt8=re.findall(r"(\d{1,2}-[A-Za-z]{2,}-\d{2})",str(item))
			dt9=re.findall(r"(\d{2}\/\d{2}\/\d{4})",str(item))
			
			urlhash=re.findall(r"(\#[A-Za-z]*)",str(item2))
			# ====================
			if urlpatt:
				urlpart=urlpatt[0]  
				if urlpart+"PAN_NO" in item:
					if panpatt:
						datadic.update({"PAN_NO":panpatt[0]})
				elif urlpart+"TOTAL" in item:
					if amount:
						datadic.update({"TOTAL":amount[0]})
						amounts.append(amount[0])
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"TOTAL":amount2[0]})
						amounts.append(amount2[0])
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"TOTAL":amount3[0]})
						amounts.append(amount3[0])
						non_headers_index.append(index)
				elif urlpart+"AMOUNT" in item:
					if amount:
						datadic.update({"TOTAL":amount[0]})
						amounts.append(amount[0])
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"TOTAL":amount2[0]})
						amounts.append(amount2[0])
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"TOTAL":amount3[0]})
						amounts.append(amount3[0])
						non_headers_index.append(index)
				elif urlpart+"Gross_Total" in item:
					if amount:
						datadic.update({"GRAND_TOTAL":amount[0]})
						amounts.append(amount[0])
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"GRAND_TOTAL":amount2[0]})
						amounts.append(amount2[0])
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"GRAND_TOTAL":amount3[0]})
						amounts.append(amount3[0])
						non_headers_index.append(index)
				elif urlpart+"Net_Amount" in item:
					if amount:
						datadic.update({"Net_Amount":amount[0]})
						amounts.append(amount[0])
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"Net_Amount":amount2[0]})
						amounts.append(amount2[0])
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"Net_Amount":amount3[0]})
						amounts.append(amount3[0])
						non_headers_index.append(index)
				elif urlpart+"SGST" in item:
					if sgst_amount:
						datadic.update({"SGST":sgst_amount[0]})
						non_headers_index.append(index)
					elif sgst_amount2:
						datadic.update({"SGST":sgst_amount2[0]})
						non_headers_index.append(index)
					elif sgst_amount3:
						datadic.update({"SGST":sgst_amount3[0]})
						non_headers_index.append(index)
					elif sgst_amount4:
						datadic.update({"SGST":sgst_amount4[0]})
						non_headers_index.append(index)
					elif amount:
						datadic.update({"SGST":amount[0]})
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"SGST":amount2[0]})
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"SGST":amount3[0]})
						non_headers_index.append(index)
					elif amount4:
						datadic.update({"SGST":amount4[0]})
						non_headers_index.append(index)
					elif cgst_amount:
						datadic.update({"SGST":cgst_amount[0]})
						non_headers_index.append(index)
					elif cgst_amount2:
						datadic.update({"SGST":cgst_amount2[0]})
						non_headers_index.append(index)
					elif cgst_amount3:
						datadic.update({"SGST":cgst_amount3[0]})
						non_headers_index.append(index)
					elif cgst_amount4:
						datadic.update({"SGST":cgst_amount4[0]})
						non_headers_index.append(index)
					
				elif urlpart+"CGST" in item:
					if cgst_amount:
						datadic.update({"CGST":cgst_amount[0]})
						non_headers_index.append(index)
					elif cgst_amount2:
						datadic.update({"CGST":cgst_amount2[0]})
						non_headers_index.append(index)
					elif cgst_amount3:
						datadic.update({"CGST":cgst_amount3[0]})
						non_headers_index.append(index)
					elif cgst_amount4:
						datadic.update({"CGST":cgst_amount4[0]})
						non_headers_index.append(index)
					elif sgst_amount:
						datadic.update({"CGST":sgst_amount[0]})
						non_headers_index.append(index)
					elif sgst_amount2:
						datadic.update({"CGST":sgst_amount2[0]})
						non_headers_index.append(index)
					elif sgst_amount3:
						datadic.update({"CGST":sgst_amount3[0]})
						non_headers_index.append(index)
					elif sgst_amount4:
						datadic.update({"CGST":sgst_amount4[0]})
						non_headers_index.append(index)
					elif amount:
						datadic.update({"CGST":amount[0]})
						non_headers_index.append(index)
					elif amount2:
						datadic.update({"CGST":amount2[0]})
						non_headers_index.append(index)
					elif amount3:
						datadic.update({"CGST":amount3[0]})
						non_headers_index.append(index)
					elif amount4:
						datadic.update({"CGST":amount4[0]})
						non_headers_index.append(index)
					
				elif urlpart+"GSTIN" in item:
					if gstin:
						if gstin!="24AAACE1741P1ZN":
							datadic.update({"GSTTIN":gstin[0]})
					
				elif urlpart+"Purchase_Order_Number" in item:
					popatt2=re.findall(r"(ESTL.*)",str(tmp_final_datalist[index+1]))
					if popatt:
						datadic.update({"PONumber":popatt[0]})
						datadic.update({"PO":popatt[0]})
					elif popatt2:
						datadic.update({"PONumber":popatt2[0]})
						datadic.update({"PO":popatt2[0]})
					
				elif urlpart+"INVOICE_NO" in item:
					if invoice:
						datadic.update({"Invoice_No":invoice[0]})
					
				elif urlpart+"Invoice_Date" in item:
					if dt:
						datadic.update({"Invoice_Date":dt[0]})
					elif dt2:
						datadic.update({"Invoice_Date":dt2[0]})
					elif dt3:
						datadic.update({"Invoice_Date":dt3[0]})
					elif dt4:
						datadic.update({"Invoice_Date":dt4[0]})
					elif dt5:
						datadic.update({"Invoice_Date":dt5[0]})
					elif dt6:
						datadic.update({"Invoice_Date":dt6[0]})
					elif dt7:
						datadic.update({"Invoice_Date":dt7[0]})
					elif dt8:
						datadic.update({"Invoice_Date":dt8[0]})
					
				# if all(i >= index for i in headers_index):
				# print ("rates--",index)
				
				# if rates:
					
				if "#UNIT" in urlhash:
					if "Unit" not in headers:
						headers.append("Unit")
						headers_index.append(index)
				if "#Quantity" in urlhash:		
					if "Quantity" not in headers:
						print ("h2---",headers)
						headers.append("Quantity")
						headers_index.append(index)
				if "#RATE" in urlhash:
					if "Rate" not in headers:
						headers.append("Rate")
						headers_index.append(index)
				if "#AMOUNT" in urlhash:
					if "Amount" not in headers:
						headers.append("Amount")
						headers_index.append(index)
				if "#HSN" in urlhash:
					if "HSN" not in headers:
						headers.append("HSN")
						headers_index.append(index)
				
				if urlpart+"TAX_CODE" in item:
					non_headers_index.append(index)
				if urlpart+"ITC_Eligible" in item:
					non_headers_index.append(index)
				if urlpart+"ITC_Not_Eligible" in item:
					non_headers_index.append(index)
				
			else:
				description.append(item)
		
		print ("headers--",headers)
		print ("hind--",headers_index)
		print ("nonhind--",non_headers_index)
		if datadic:
			print ("dic--",datadic)
	if "Vendor_Name" not in datadic.keys():
		datadic.update({"Vendor_Name":""})
	if "itemdescription" not in datadic.keys():
		datadic.update({"itemdescription":[]})
	if "IGST" not in datadic.keys():
		datadic.update({"IGST":""})
	if "ServiceTaxNo" not in datadic.keys():
		datadic.update({"ServiceTaxNo":""})
	if "GRN" not in datadic.keys():
		datadic.update({"GRN":""})
	if "TAN_NO" not in datadic.keys():
		datadic.update({"TAN_NO":""})
	if "Bill_To" not in datadic.keys():
		datadic.update({"Bill_To":""})
	if "Bill_To_Plant" not in datadic.keys():
		datadic.update({"Bill_To_Plant":""})
	if "Ship_To_Plant" not in datadic.keys():
		datadic.update({"Ship_To_Plant":""})
	if "Ship_To" not in datadic.keys():
		datadic.update({"Ship_To":""})
	if "PONumber" not in datadic.keys():
		datadic.update({"PONumber":""})
	if "PO" not in datadic.keys():
		datadic.update({"PO":""})
	if "Invoice_No" not in datadic.keys():
		datadic.update({"Invoice_No":""})
	if "Invoice_Date" not in datadic.keys():
		datadic.update({"Invoice_Date":""})
	if "PAN_NO" not in datadic.keys():
		datadic.update({"PAN_NO":""})
	if "GSTTIN" not in datadic.keys():
		datadic.update({"GSTTIN":""})
	if "CGST" not in datadic.keys():
		datadic.update({"CGST":""})
	if "SGST" not in datadic.keys():
		datadic.update({"SGST":""})
	if "GRAND_TOTAL" not in datadic.keys():
		datadic.update({"GRAND_TOTAL":""})
	# return datadic
	return tmp_final_datalist
if __name__=="__main__":
	app.run(host="0.0.0.0",debug=True,port=5018)
	