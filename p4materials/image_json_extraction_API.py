import os
import requests
import json
import re
import copy
from flask import Flask,jsonify,request
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = './input_imagejsons'
urlhash=r'(\#[A-Za-z].*)'
URL='http://35.186.166.22:8082/portal/servlet/service/Poheader.poi'
FURL='http://34.80.26.185:8086/PO_Processing/GetDataSolr.flatteringTemp'

URLS_JSON={
			
			'http://bizlem.io/PurchaseOrderProcessing#SGST':'Integer/Float',
			'http://bizlem.io/PurchaseOrderProcessing#IGST':'Integer/Float',
			'http://bizlem.io/PurchaseOrderProcessing#Quantity':'Integer',
			'http://bizlem.io/PurchaseOrderProcessing#Discount':'Integer/Float',
			'http://bizlem.io/PurchaseOrderProcessing#HSN/SAC':'Integer/Float',
			'http://bizlem.io/PurchaseOrderProcessing#SNo':'Integer',
			'http://bizlem.io/PurchaseOrderProcessing#DescriptionOfServices':'string',
			'http://bizlem.io/PurchaseOrderProcessing#Unit':'Integer',
			'http://bizlem.io/PurchaseOrderProcessing#Rate':'Float',
			}

app=Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

if not os.path.exists(UPLOAD_FOLDER):
	os.mkdir(UPLOAD_FOLDER)

def allowed_file(filename, extensions):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in extensions
	
@app.route('/image_json',methods=['POST'])
def update_json():
	files=request.files
	sdata=request.data
	print ('f--',files)
	final_json=[]
	
	if 'file' not in files:
		if sdata:
			image_json=json.loads(sdata)
		else:
			return jsonify({'error':'No files provided'}), 400	
	elif 'file' in files:
		file = request.files['file']
		if file and allowed_file(file.filename, ['json']):
			filename = secure_filename(file.filename)
			path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
			file.save(path)
			with open(path) as f:
				image_json=json.load(f)
	
	final_json={}
	headers=[]
	
	if image_json:				
		if 'header_info' in image_json.keys():		
			if 'header_line_index' in image_json['header_info']:
				headers_index=image_json['header_info']['header_line_index']
			if headers_index:
				headers_index.append(min(headers_index)-1)							
		if 'concatenation' in image_json.keys():
			for itm in image_json['concatenation']:
				if itm['line_index'] in headers_index:
					if 'words' in itm.keys():
						for xy in itm['words']:										
							xy.update({'y':itm['y'],'line_index':itm['line_index']})
							headers.append(xy)
	# ----------------result on xsorted json------------------------------------------------
	headers_xsorted=sorted(headers,key=lambda i: i['x1'])
	for index,item in enumerate(headers_xsorted):
		prefix='a'+str(index+1)
		item.update({prefix:prefix})
		if 'url' in item.keys():		
			if isinstance(item['url'], str):
				urlpatt=re.findall(urlhash,item['url'])
				if urlpatt:				
					item.update({'url_found':'Y'})	
			else:
				item.update({'url_found':'N'})
				
		if index>0:
			item.update({'lo':str(int(item['line_index'])-int(headers_xsorted[index-1]['line_index']))})
			item.update({'x_offset':str(int(item['x1'])-int(headers_xsorted[index-1]['x2']))})
		else:
			item.update({'lo':str(0),'x_offset':str(0)})
					
	# change key prefixes	
	for index,item in enumerate(headers_xsorted):
		prefix='a'+str(index+1)
		item[prefix+'_line_index']=item.pop('line_index',item['line_index'])
		item[prefix+'_lo']=item.pop('lo',item['lo'])
		item[prefix+'_url']=item.pop('url',item['url'])
		item[prefix+'_url_found']=item.pop('url_found',item['url_found'])
		item[prefix+'_word']=item.pop('word',item['word'])
		item[prefix+'_x1']=item.pop('x1',item['x1'])
		item[prefix+'_x2']=item.pop('x2',item['x2'])
		item[prefix+'_y']=item.pop('y',item['y'])
		item[prefix+'_x_offset']=item.pop('x_offset',item['x_offset'])
		item[prefix+'_type']=item.pop('type',item['type'])
		# convert values to string
		item[prefix+'_line_index']=str(item[prefix+'_line_index'])
		item[prefix+'_lo']=str(item[prefix+'_lo'])
		item[prefix+'_url']=str(item[prefix+'_url'])
		item[prefix+'_url_found']=str(item[prefix+'_url_found'])
		item[prefix+'_word']=str(item[prefix+'_word'])
		item[prefix+'_x1']=str(item[prefix+'_x1'])
		item[prefix+'_x2']=str(item[prefix+'_x2'])
		item[prefix+'_y']=str(item[prefix+'_y'])
		item[prefix+'_x_offset']=str(item[prefix+'_x_offset'])
		item[prefix+'_type']=str(item[prefix+'_type'])
	
	combined_final_json={}
	# copy_1=copy.deepcopy(headers_xsorted)
	# combined_final_json.update({'step_1':copy_1})
	if len(headers_xsorted)>1:
		final_json=combine(0,1,headers_xsorted)
	
	# call flatening API
	if final_json:
		final_json=flattening_API(final_json)
		
	combined_final_json.update({'header_transform_step1':final_json})
	
	return jsonify(combined_final_json)
	
def combine(current,next,headers_xsorted):
	# rename keys to a1 for current and a2 for next
	current_prefix=list(headers_xsorted[current].keys())[0].split('_')[0]
	next_prefix=list(headers_xsorted[next].keys())[0].split('_')[0]
	
	combined_json1=headers_xsorted[current].copy()
	combined_json2=headers_xsorted[next].copy()
	# print ('combined_json1---',combined_json1)
	# print ('combined_json2---',combined_json2)
	
	combined_json1['a1_x1']=combined_json1.pop(current_prefix+'_x1',combined_json1[current_prefix+'_x1'])
	combined_json1['a1_x2']=combined_json1.pop(current_prefix+'_x2',combined_json1[current_prefix+'_x2'])
	combined_json1['a1_word']=combined_json1.pop(current_prefix+'_word',combined_json1[current_prefix+'_word'])
	combined_json1['a1_lo']=combined_json1.pop(current_prefix+'_lo',combined_json1[current_prefix+'_lo'])
	combined_json1['a1_url']=combined_json1.pop(current_prefix+'_url',combined_json1[current_prefix+'_url'])
	combined_json1['a1_url_found']=combined_json1.pop(current_prefix+'_url_found',combined_json1[current_prefix+'_url_found'])
	combined_json1['a1_y']=combined_json1.pop(current_prefix+'_y',combined_json1[current_prefix+'_y'])
	combined_json1['a1_x_offset']=combined_json1.pop(current_prefix+'_x_offset',combined_json1[current_prefix+'_x_offset'])
	combined_json1['a1_line_index']=combined_json1.pop(current_prefix+'_line_index',combined_json1[current_prefix+'_line_index'])
	combined_json1['a1_type']=combined_json1.pop(current_prefix+'_type',combined_json1[current_prefix+'_type'])
	
	combined_json2['a2_x1']=combined_json2.pop(next_prefix+'_x1',combined_json2[next_prefix+'_x1'])
	combined_json2['a2_x2']=combined_json2.pop(next_prefix+'_x2',combined_json2[next_prefix+'_x2'])
	combined_json2['a2_word']=combined_json2.pop(next_prefix+'_word',combined_json2[next_prefix+'_word'])
	combined_json2['a2_lo']=combined_json2.pop(next_prefix+'_lo',combined_json2[next_prefix+'_lo'])
	combined_json2['a2_url']=combined_json2.pop(next_prefix+'_url',combined_json2[next_prefix+'_url'])
	combined_json2['a2_url_found']=combined_json2.pop(next_prefix+'_url_found',combined_json2[next_prefix+'_url_found'])
	combined_json2['a2_y']=combined_json2.pop(next_prefix+'_y',combined_json2[next_prefix+'_y'])
	combined_json2['a2_x_offset']=combined_json2.pop(next_prefix+'_x_offset',combined_json2[next_prefix+'_x_offset'])
	combined_json2['a2_line_index']=combined_json2.pop(next_prefix+'_line_index',combined_json2[next_prefix+'_line_index'])
	combined_json2['a2_type']=combined_json2.pop(next_prefix+'_type',combined_json2[next_prefix+'_type'])
	
	
	combined_json1.update(combined_json2)
	
	strj={"user_name":"carrotrule_xyz.com","project_name":"TableHeadingPO","Rule_Engine":"HeadingPORule","RawJson": combined_json1}
	print ('strj--',strj)
	returned_json=requests.post(URL,data=json.dumps(strj)).json()
	# final_json={}
	print ('combined_json---',returned_json)
	# # print ('curr--',headers_xsorted[current])
	# # print ('next--',headers_xsorted[next])
	# print ('===============')
	if 'Merged' in returned_json.keys():
		if returned_json['Merged']=='True':
			print ('mjson--',returned_json['Merged'])
			merged_json=mergejson(current,next,headers_xsorted)
			if current<=len(merged_json)-2:
				# print ('current3--',current)
				final_json=combine(current,current+1,merged_json)
			else:
				return merged_json
				
		else:
			if next<=len(headers_xsorted)-2:
				final_json=combine(next,next+1,headers_xsorted)
			else:
				return headers_xsorted
					
	return headers_xsorted

def mergejson(current,next,headers_xsorted):
	current_prefix=list(headers_xsorted[current].keys())[0].split('_')[0]
	next_prefix=list(headers_xsorted[next].keys())[0].split('_')[0]
	# print ('cpx--',current_prefix)
	# print ('nxt--',next_prefix)
	# print ('curr--',headers_xsorted[current])
	# print ('next--',headers_xsorted[next])
	# merge values of current into next
	if int(headers_xsorted[current][current_prefix+'_x1'])<int(headers_xsorted[next][next_prefix+'_x1']):
		headers_xsorted[next][next_prefix+'_x1']=headers_xsorted[current][current_prefix+'_x1']
	# headers_xsorted[next][next_prefix+'_word']=headers_xsorted[current][current_prefix+'_word']+"@@@@@"+headers_xsorted[next][next_prefix+'_word']
	headers_xsorted[next][next_prefix+'_word']=headers_xsorted[current][current_prefix+'_word']+headers_xsorted[next][next_prefix+'_word']
	del headers_xsorted[current]
	return headers_xsorted

def flattening_API(final_json):
	for index,item in enumerate(final_json):
		prefix=list(final_json[index].keys())[0].split('_')[0]
		val=str(final_json[index][prefix+'_word']) 
		result=requests.post(FURL,data=json.dumps({'H4':val,'H3': '', 'H2': ' ', 'H1': ' '}))
		r_json=result.json()
		# print ('rjson--',r_json)
		if r_json:
			if 'response' in r_json.keys():
				# print ('r---',r_json['response'])
				if 'docs' in r_json['response'].keys():
					for ind,im in enumerate(r_json['response']['docs']):
						if 'url' in im.keys():
							if im['url']:
								urlval=im['url'][0]
								print ('uval--',val,urlval)
								item.update({'header_URL':urlval,'header_word':val})
							
		
	return final_json
	
if __name__=='__main__':
	app.run(host='0.0.0.0',debug=True,port=5014)