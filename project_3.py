import pandas as pd
import math
import requests
import threading


URL = 'http://35.188.227.39:8080/enhancer/chain/EssarApproval'
LABEL = 'http://bizlem.io/EssarApproval'


def request_title(s, labels):
    if 'unknown' in s:
        labels[s] = s
        return

    r = requests.post(URL, data=s, headers={'Content-Type': 'application/pdf'})
    res = r.json()

    label = None
    for _, v in res[0].items():
        if LABEL in v:
            label = v
            break
    
    labels[s] = label


def process_table(table, table_name, container):
    table.dropna(how='all', inplace=True)
    header_index = 0
    height, width = table.shape

    while header_index < height:
        c_nan = table.iloc[header_index].isna().sum()
        if c_nan / width < 0.7:
            break

        header_index += 1
    
    if header_index >= height:
        return 

    columns = table.iloc[header_index].tolist()

    # set unknown labels
    c = 0
    for i in range(0, width):
        if pd.isnull(columns[i]):
            columns[i] = 'unknown_' + str(c)
            c += 1

    table.columns = columns
    table.drop(index=table.index[:header_index + 1], inplace=True)
    print(table.columns)

    labels = {}
    threads = [ threading.Thread(target=request_title, args=(c, labels)) for c in table.columns ]

    print('Requesting labels ...')
    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
    
    # set unknown labels after requesting api
    for k, v in labels.items():
        if pd.isnull(v):
            labels[k] = 'unknown_' + str(c)
            c += 1

    table.rename(columns=labels, inplace=True)
    print(labels)
    print(table.columns)

    results = table.astype(str).to_dict(orient='index')
    
    container[table_name] = list(results.values())


def process_excel(path):
    # t = pd.read_excel(path, header=None, sheet_name=0).head(20)
    # print(t)
    # container = {}
    # process_table(t, 'hi', container)
    # return container

    tables = pd.read_excel(path, header=None, sheet_name=None)
   
    container = {}
    threads = [ threading.Thread(target=process_table, args=(table, table_name, container)) for table_name, table in tables.items() ]

    # Processing tables...
    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    return container
    
if __name__ == '__main__':
    # process_excel('Excel Parsing/sample excels/excel10.xlsx')
    # exit()

    import os
    files = os.listdir('Excel Parsing/sample excels')
    for f in files:
        print(f)
        process_excel(os.path.join('Excel Parsing/sample excels', f))
