import requests
import json
import pandas as pd
import re
import threading


URL = 'http://35.188.227.39:8080/enhancer/chain/EssarApproval'
TITLE = 'http://www.w3.org/2000/01/rdf-schema#label'

DIFF_LABEL = 'Diff_Surcharge_Discount_toaddinthesystem'

def is_int(s):
    try:
        num = int(s)
        return True
    except ValueError:
        return False

def is_str(s):
    return isinstance(s, str)

def strip(s):
    return ''.join(re.split('[^a-zA-Z]', s.lower()))

def to_float(s):
    if isinstance(s, str):
        sign = 1
        if re.search('\([\d\,\.]+\)', s) is not None:
            sign = -1
            
        return float(''.join(re.split('[^0-9\.\-]', s))) * sign
    return s

# crawl variation of labels
print('Requesting server ...')
default_labels = [
    'SO',
    'Customer',
    'Grade',
    'Item',
    'Product',
    'Entry date',
    'Qty',
    'Thick',
    'Width',
    'Delivered',
    'System NSR',
    'Ex-Hazira',
]

labels = {}

def request_label(label):
    r = requests.post(URL, data=label, headers={'Content-Type': 'application/pdf'})
    res = r.json()

    if TITLE in res[0]:
        for v in res[0][TITLE]:
            labels[strip(v['@value'])] = label


threads = [ threading.Thread(target=request_label, args=(label,)) for label in default_labels ]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

labels['proposaldt'] = 'Entry date'
labels['proposaldate'] = 'Entry date'
labels['proposedt'] = 'Entry date'
labels['proposedate'] = 'Entry date'
labels[strip(DIFF_LABEL)] = DIFF_LABEL


# set header 

def set_specific_header(table):
    _, width = table.shape

    hazira = False # to detect delivered price
    for i in range(width):
        col = table.iloc[0, i]
        if is_str(col) and 'hazira' in strip(col):
            table.iloc[0, i] = 'System_NSR'
            hazira = True
            break

    if not hazira:
        for i in range(width):
            col = table.iloc[0, i]
            if is_str(col) and 'del' in strip(col):
                table.iloc[0, i] = 'System_NSR'
                break
    
    table.columns = table.iloc[0]


def set_html_header(table):
    table.dropna(axis=0, how='all', inplace=True)
    table.dropna(axis=1, how='all', inplace=True)

    j = 0
    height, width = table.shape
    while j < width and 'As' not in table.iloc[0, j]:
        j += 1

    i = 0
    # detect whether 2 headers the same
    if table.iloc[0, i] == table.iloc[1, i]:
        table.iloc[0] = table.iloc[1]
    else:
        end_index = width - 1
        while end_index >= 0 and pd.isnull(table.iloc[0, end_index]):
            end_index -= 1
        temp = table.iloc[0, j:end_index + 1].tolist()

        while i + j < width:
            if pd.isnull(table.iloc[1, i]):
                break

            table.iloc[0, i + j] = table.iloc[1, i]
            i += 1

        k = 1
        i += j
        while k <= width - i:
            table.iloc[0, -k] = temp[-k]
            k += 1

    set_specific_header(table)

      
def set_excel_header(table):
    height, width = table.shape

    for i in range(1, height):
        isna = table.iloc[i].isna()
        if isna.sum() / width <= 0.15:
            break
        
        for start in range(width):
            if not pd.isnull(table.iloc[i, start]):
                table.iloc[0, start] = table.iloc[i, start]

    set_specific_header(table)
    
    
def find_first_index(table):
    # find index of the first row of data
    start = 0
    label_so = None

    for col in table.columns:
        for k, v in labels.items():
            if v == 'SO' and is_str(col) and k == strip(col):
                label_so = col
                break

    while start < len(table):
        if is_int(table[label_so].iloc[start]):
            break
        else:
            start += 1

    return start, label_so

def p1_process(path, verbose=True):
    ext = path.split('.')[-1]
    if ext == 'html':
        return p1_process_html(path, verbose=verbose)
    if ext == 'xls' or ext == 'xlsx':
        return p1_process_excel(path, verbose)


def p1_process_html(path, verbose=True):
    tables = pd.read_html(path)

    table = None
    for t in tables:
        th, tw = t.shape
        if tw > 5:
            table = t
            break

    set_html_header(table)
    start, label_so = find_first_index(table)

    if verbose:
        print(table.columns)

    return process_table(table, start, label_so, verbose=verbose)

def p1_process_excel(path, verbose=True):
    xls = pd.ExcelFile(path)
    sheets = xls.book.sheets()

    # read visibble sheet only
    tables = {}
    for sheet in sheets:
        if sheet.visibility == 0:
            table = pd.read_excel(xls, sheet.name, header=None)
            height, width = table.shape
            if width >= 5:
                tables[sheet.name] = table
            
    container = { 'SOTable': [] }
    for table_name, table in tables.items():
        if verbose:
            print(table_name)
        table.dropna(axis=0, how='all', inplace=True)
        table.dropna(axis=1, how='all', inplace=True)

        set_excel_header(table)
        start, label_so = find_first_index(table)

        if verbose:
            print(table.columns)

        result = process_table(table, start, label_so, verbose=verbose)
        container['SOTable'] += result['SOTable']

    return container

    
# parse data
def process_table(table, start, label_so, verbose=True):
    results = []

    i = start
    height, width = table.shape

    for i in range(start, height):
        if is_int(table[label_so].iloc[i]):
            obj = {}
            
            for col in table.columns:
                if not is_str(col):
                    continue

                if strip(col) in labels:
                    if strip(col) == 'systemnsr':
                        obj['System_NSR'] = to_float(table[col].iloc[i])
                    elif 'diff' in strip(col) and 'surcharge'in strip(col):
                        obj[DIFF_LABEL] = to_float(table[col].iloc[i])
                    elif not 'del' in strip(col):
                        obj[labels[strip(col)]] = table[col].iloc[i]
                else:
                    if 'diff' in strip(col) and DIFF_LABEL not in obj:
                        obj[DIFF_LABEL] = to_float(table[col].iloc[i])

           
            obj['Proposed_NSR'] = obj[DIFF_LABEL] + obj['System_NSR']

            results.append(obj)


    final = { 'SOTable': results }
    if verbose:
        print('Found', len(results), 'record(s)')
    return final


if __name__ == '__main__':
    # r = p1_process('p1materials/input9.html')
    r = p1_process('p1materials/input14.xls')
    print(json.dumps(r, indent=2))
